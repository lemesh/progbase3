// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ReinessanceGameMode.generated.h"

UCLASS(minimalapi)
class AReinessanceGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AReinessanceGameMode();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Quest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Experience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MaxExperience;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int ClassSelected;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int AmountFound;
};



