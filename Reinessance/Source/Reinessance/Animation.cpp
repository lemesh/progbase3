// Fill out your copyright notice in the Description page of Project Settings.

#include "Animation.h"

void UAnimation::UpdateAnimationProperties() {
	APawn* Pawn = TryGetPawnOwner();

	if (Pawn) {
		Direction = CalculateDirection(Pawn->GetVelocity(), Pawn->GetActorRotation());
		Speed = Pawn->GetVelocity().SizeSquared();
	}
}


