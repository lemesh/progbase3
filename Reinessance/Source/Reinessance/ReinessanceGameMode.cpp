// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ReinessanceGameMode.h"
#include "ReinessancePlayerController.h"
#include "ReinessanceCharacter.h"
#include "UObject/ConstructorHelpers.h"

AReinessanceGameMode::AReinessanceGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AReinessancePlayerController::StaticClass();

	Quest = 0;
	Experience = 0;
	MaxExperience = 5;
	Level = 1;
	ClassSelected = 0;
}