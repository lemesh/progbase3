// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Animation.generated.h"

/**
 * 
 */
UCLASS()
class REINESSANCE_API UAnimation : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Direction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Speed;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	bool Casting1HAnim;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	bool Casting1HCombatAnim;

	UFUNCTION(BlueprintCallable, Category = "UpdateAnimationProperties")
	void UpdateAnimationProperties();
	
};
