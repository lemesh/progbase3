// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ReinessanceCharacter.generated.h"

UCLASS(Blueprintable)
class AReinessanceCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AReinessanceCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Mana;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	bool Casting1H;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool Casting1HCombat;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HealthRegen;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ManaRegen;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float HealthRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ManaRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Spell1CD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Spell2CD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool SlashSword;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool HitSword;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int MagicCurrency;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int SwordCurrency;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool iClicked;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	
};

